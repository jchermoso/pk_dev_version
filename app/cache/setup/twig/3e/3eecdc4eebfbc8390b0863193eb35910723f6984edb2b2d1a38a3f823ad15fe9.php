<?php

/* PartKeeprSetupBundle::parameters.php.twig */
class __TwigTemplate_7b28533c84e99b7abd070c6e66e144e3009c1c7e54df9218f15398e97f8eeebb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PartKeeprSetupBundle::parameters.php.twig"));

        // line 1
        echo "<?php
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["parameters"] ?? null));
        foreach ($context['_seq'] as $context["name"] => $context["value"]) {
            // line 3
            echo "\$container->setParameter('";
            echo twig_escape_filter($this->env, $context["name"], "html", null, true);
            echo "', ";
            echo $context["value"];
            echo ");
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PartKeeprSetupBundle::parameters.php.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php
{% for name,value in parameters %}
\$container->setParameter('{{ name }}', {{ value|raw }});
{% endfor %}
", "PartKeeprSetupBundle::parameters.php.twig", "/var/www/pk/src/PartKeepr/SetupBundle/Resources/views/parameters.php.twig");
    }
}
