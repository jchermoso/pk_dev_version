<?php

/* PartKeeprDoctrineReflectionBundle::model.js.twig */
class __TwigTemplate_7ced24f0e797f47cfde8d54e61ab87a9387ae7eded967734a42c5d86cbf8c84f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PartKeeprDoctrineReflectionBundle::model.js.twig"));

        // line 1
        echo "Ext.define('";
        echo twig_escape_filter($this->env, ($context["className"] ?? null), "js", null, true);
        echo "', {
    extend: '";
        // line 2
        echo twig_escape_filter($this->env, ($context["parentClass"] ?? null), "js", null, true);
        echo "',
    alias: 'schema.";
        // line 3
        echo twig_escape_filter($this->env, ($context["className"] ?? null), "js", null, true);
        echo "',

    idProperty: \"@id\",
    fields: [
        ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["fields"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 8
            echo "        { name: '";
            echo $this->getAttribute($context["field"], "name", array());
            echo "'";
            if ($this->getAttribute($context["field"], "type", array())) {
                echo ", type: '";
                echo twig_escape_filter($this->env, $this->getAttribute($context["field"], "type", array()), "js", null, true);
                echo "'";
            }
            if ($this->getAttribute($context["field"], "nullable", array())) {
                echo ", allowNull: true";
            }
            if ( !$this->getAttribute($context["field"], "persist", array())) {
                echo ", persist: false";
            }
            if ($this->getAttribute($context["field"], "validators", array())) {
                echo ", validators: ";
                echo $this->getAttribute($context["field"], "validators", array());
            }
            echo "}";
            if ( !$this->getAttribute($context["loop"], "last", array())) {
                echo ",";
            }
            // line 9
            echo "
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute(($context["associations"] ?? null), "MANY_TO_ONE", array())) > 0)) {
            // line 12
            echo "            ,
            ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["associations"] ?? null), "MANY_TO_ONE", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["association"]) {
                // line 14
                echo "                { name: '";
                echo twig_escape_filter($this->env, $this->getAttribute($context["association"], "name", array()), "js", null, true);
                echo "',
                reference: '";
                // line 15
                echo twig_escape_filter($this->env, $this->getAttribute($context["association"], "target", array()), "js", null, true);
                echo "',
            allowBlank: ";
                // line 16
                if ($this->getAttribute($context["association"], "nullable", array())) {
                    echo "true";
                } else {
                    echo "false";
                }
                // line 17
                echo "        ";
                if ($this->getAttribute($context["association"], "byReference", array())) {
                    echo ",byReference: true";
                }
                // line 18
                echo "                }";
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ",";
                }
                // line 19
                echo "
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['association'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "        ";
        }
        // line 22
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute(($context["associations"] ?? null), "ONE_TO_ONE", array())) > 0)) {
            // line 23
            echo "            ,
            ";
            // line 24
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["associations"] ?? null), "ONE_TO_ONE", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["association"]) {
                // line 25
                echo "                { name: '";
                echo twig_escape_filter($this->env, $this->getAttribute($context["association"], "name", array()), "js", null, true);
                echo "',
                reference: '";
                // line 26
                echo twig_escape_filter($this->env, $this->getAttribute($context["association"], "target", array()), "js", null, true);
                echo "'
                }";
                // line 27
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ",";
                }
                // line 28
                echo "
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['association'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "        ";
        }
        // line 31
        echo "
    ],

    ";
        // line 34
        if ((twig_length_filter($this->env, $this->getAttribute(($context["associations"] ?? null), "ONE_TO_MANY", array())) > 0)) {
            // line 35
            echo "    hasMany: [
    ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["associations"] ?? null), "ONE_TO_MANY", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["association"]) {
                // line 37
                echo "        {
        name: '";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute($context["association"], "name", array()), "js", null, true);
                echo "',
        associationKey: '";
                // line 39
                echo twig_escape_filter($this->env, $this->getAttribute($context["association"], "name", array()), "js", null, true);
                echo "',
        model: '";
                // line 40
                echo twig_escape_filter($this->env, $this->getAttribute($context["association"], "target", array()), "js", null, true);
                echo "'
        }";
                // line 41
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ",";
                }
                // line 42
                echo "
    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['association'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "    ],
    ";
        }
        // line 46
        echo "
    ";
        // line 47
        if ((twig_length_filter($this->env, $this->getAttribute(($context["associations"] ?? null), "MANY_TO_MANY", array())) > 0)) {
            // line 48
            echo "    manyToMany: {
    ";
            // line 49
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["associations"] ?? null), "MANY_TO_MANY", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["association"]) {
                // line 50
                echo "        ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["association"], "name", array()), "js", null, true);
                echo ": {
            type: '";
                // line 51
                echo twig_escape_filter($this->env, $this->getAttribute($context["association"], "target", array()), "js", null, true);
                echo "',
            role: '";
                // line 52
                echo twig_escape_filter($this->env, $this->getAttribute($context["association"], "name", array()), "js", null, true);
                echo "',
            field: '@id',
            right: true
        } ";
                // line 55
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ",";
                }
                // line 56
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['association'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "    },
    ";
        }
        // line 59
        echo "
    proxy: {
        type: \"Hydra\",
        url: '";
        // line 62
        if ((($context["uri"] ?? null) == "")) {
            echo "undefined:";
            echo twig_escape_filter($this->env, ($context["className"] ?? null), "js", null, true);
        } else {
            echo ($context["uri"] ?? null);
        }
        echo "'
        ";
        // line 63
        if ((($context["ignoreIds"] ?? null) == true)) {
            // line 64
            echo "        , ignoreIds: true
        ";
        }
        // line 66
        echo "    }
});

PartKeepr.Data.Store.ModelStore.addModel('";
        // line 69
        echo twig_escape_filter($this->env, ($context["className"] ?? null), "js", null, true);
        echo "', '');
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PartKeeprDoctrineReflectionBundle::model.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  365 => 69,  360 => 66,  356 => 64,  354 => 63,  345 => 62,  340 => 59,  336 => 57,  322 => 56,  318 => 55,  312 => 52,  308 => 51,  303 => 50,  286 => 49,  283 => 48,  281 => 47,  278 => 46,  274 => 44,  259 => 42,  255 => 41,  251 => 40,  247 => 39,  243 => 38,  240 => 37,  223 => 36,  220 => 35,  218 => 34,  213 => 31,  210 => 30,  195 => 28,  191 => 27,  187 => 26,  182 => 25,  165 => 24,  162 => 23,  159 => 22,  156 => 21,  141 => 19,  136 => 18,  131 => 17,  125 => 16,  121 => 15,  116 => 14,  99 => 13,  96 => 12,  93 => 11,  78 => 9,  55 => 8,  38 => 7,  31 => 3,  27 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("Ext.define('{{ className }}', {
    extend: '{{ parentClass }}',
    alias: 'schema.{{ className }}',

    idProperty: \"@id\",
    fields: [
        {% for field in fields %}
        { name: '{{ field.name|raw }}'{% if field.type%}, type: '{{ field.type }}'{% endif %}{% if field.nullable%}, allowNull: true{% endif %}{% if not field.persist %}, persist: false{% endif %}{% if field.validators %}, validators: {{ field.validators|raw }}{% endif %}}{% if not loop.last %},{% endif %}

        {% endfor %}
        {% if associations.MANY_TO_ONE|length > 0 %}
            ,
            {% for association in associations.MANY_TO_ONE %}
                { name: '{{ association.name }}',
                reference: '{{ association.target }}',
            allowBlank: {% if association.nullable %}true{% else %}false{% endif %}
        {% if association.byReference %},byReference: true{% endif %}
                }{% if not loop.last %},{% endif %}

            {% endfor %}
        {% endif %}
        {% if associations.ONE_TO_ONE|length > 0 %}
            ,
            {% for association in associations.ONE_TO_ONE %}
                { name: '{{ association.name }}',
                reference: '{{ association.target }}'
                }{% if not loop.last %},{% endif %}

            {% endfor %}
        {% endif %}

    ],

    {% if associations.ONE_TO_MANY|length > 0 %}
    hasMany: [
    {% for association in associations.ONE_TO_MANY %}
        {
        name: '{{ association.name }}',
        associationKey: '{{ association.name }}',
        model: '{{ association.target }}'
        }{% if not loop.last %},{% endif %}

    {% endfor %}
    ],
    {% endif %}

    {% if associations.MANY_TO_MANY|length > 0 %}
    manyToMany: {
    {% for association in associations.MANY_TO_MANY %}
        {{ association.name }}: {
            type: '{{ association.target }}',
            role: '{{ association.name }}',
            field: '@id',
            right: true
        } {% if not loop.last %},{% endif %}
    {% endfor %}
    },
    {% endif %}

    proxy: {
        type: \"Hydra\",
        url: '{% if uri == \"\" %}undefined:{{ className }}{% else %}{{ uri|raw }}{% endif %}'
        {% if ignoreIds == true %}
        , ignoreIds: true
        {% endif %}
    }
});

PartKeepr.Data.Store.ModelStore.addModel('{{ className }}', '');
", "PartKeeprDoctrineReflectionBundle::model.js.twig", "/var/www/pk/src/PartKeepr/DoctrineReflectionBundle/Resources/views/model.js.twig");
    }
}
