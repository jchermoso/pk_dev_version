<?php

/* SpriteGeneratorBundle::plainCss.html.twig */
class __TwigTemplate_a02f3e4220ee1f6610f382eb32294e9e38fc17b7e5e0772cbb062a9544ee84ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SpriteGeneratorBundle::plainCss.html.twig"));

        // line 1
        echo "
.";
        // line 2
        echo twig_escape_filter($this->env, ($context["spriteClass"] ?? null), "html", null, true);
        echo " {
    background: url(";
        // line 3
        echo twig_escape_filter($this->env, ($context["spriteImageName"] ?? null), "html", null, true);
        echo ") no-repeat;
}

";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["images"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["image"]) {
            // line 7
            echo ".";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo " {
    width: ";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "width", array()), "html", null, true);
            echo "px;
    height: ";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "height", array()), "html", null, true);
            echo "px;
    background-position: -";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "pos_x", array()), "html", null, true);
            echo "px -";
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "pos_y", array()), "html", null, true);
            echo "px !important; 
}
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SpriteGeneratorBundle::plainCss.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 10,  48 => 9,  44 => 8,  39 => 7,  35 => 6,  29 => 3,  25 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
.{{ spriteClass }} {
    background: url({{ spriteImageName }}) no-repeat;
}

{% for key, image in images %}
.{{ key }} {
    width: {{ image.width }}px;
    height: {{ image.height }}px;
    background-position: -{{ image.pos_x }}px -{{ image.pos_y }}px !important; 
}
{% endfor %}
", "SpriteGeneratorBundle::plainCss.html.twig", "/var/www/pk/vendor/nfq-alpha/sprite-bundle/src/SpriteGenerator/Resources/views/plainCss.html.twig");
    }
}
