<?php

/* PartKeeprFrontendBundle::maintenance.html.twig */
class __TwigTemplate_3490df857fdb9edd2038dfaecbdffe78ba5111c6397fe7c04a6567336dbac486 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PartKeeprFrontendBundle::maintenance.html.twig"));

        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"
        \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">
<html>
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
    <title>";
        // line 6
        echo twig_escape_filter($this->env, ($context["maintenanceTitle"] ?? null), "html", null, true);
        echo "</title>
</head>
<body>
    <h1>";
        // line 9
        echo twig_escape_filter($this->env, ($context["maintenanceTitle"] ?? null), "html", null, true);
        echo "</h1>

    <p>";
        // line 11
        echo twig_escape_filter($this->env, ($context["maintenanceMessage"] ?? null), "html", null, true);
        echo "</p>
</body>
</html>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PartKeeprFrontendBundle::maintenance.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 11,  35 => 9,  29 => 6,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"
        \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">
<html>
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>
    <title>{{ maintenanceTitle }}</title>
</head>
<body>
    <h1>{{ maintenanceTitle }}</h1>

    <p>{{ maintenanceMessage }}</p>
</body>
</html>
", "PartKeeprFrontendBundle::maintenance.html.twig", "/var/www/pk/src/PartKeepr/FrontendBundle/Resources/views/maintenance.html.twig");
    }
}
