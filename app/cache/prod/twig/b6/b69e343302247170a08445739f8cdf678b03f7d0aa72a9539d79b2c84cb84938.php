<?php

/* PartKeeprSetupBundle::authkey.php.twig */
class __TwigTemplate_0192f009ac9bdfcf617977978819bbd6938873cd37ff34e0d65d06223f5ce9a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PartKeeprSetupBundle::authkey.php.twig"));

        // line 1
        echo "<?php
/**
 * Your auth key is: ";
        // line 3
        echo twig_escape_filter($this->env, ($context["authkey"] ?? null), "html", null, true);
        echo "
 *
 * Copy and paste the auth key in order to proceed with setup
 */
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "PartKeeprSetupBundle::authkey.php.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php
/**
 * Your auth key is: {{ authkey }}
 *
 * Copy and paste the auth key in order to proceed with setup
 */
", "PartKeeprSetupBundle::authkey.php.twig", "/var/www/pk/src/PartKeepr/SetupBundle/Resources/views/authkey.php.twig");
    }
}
