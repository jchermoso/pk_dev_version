<?php

namespace Proxies\__CG__\PartKeepr\PartBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class PartDistributor extends \PartKeepr\PartBundle\Entity\PartDistributor implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'part', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'distributor', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'orderNumber', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'packagingUnit', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'price', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'currency', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'sku', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'ignoreForReports');
        }

        return array('__isInitialized__', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'part', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'distributor', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'orderNumber', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'packagingUnit', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'price', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'currency', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'sku', '' . "\0" . 'PartKeepr\\PartBundle\\Entity\\PartDistributor' . "\0" . 'ignoreForReports');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (PartDistributor $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function isIgnoreForReports()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isIgnoreForReports', array());

        return parent::isIgnoreForReports();
    }

    /**
     * {@inheritDoc}
     */
    public function setIgnoreForReports($ignoreForReports)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIgnoreForReports', array($ignoreForReports));

        return parent::setIgnoreForReports($ignoreForReports);
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrency()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCurrency', array());

        return parent::getCurrency();
    }

    /**
     * {@inheritDoc}
     */
    public function setCurrency($currency)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCurrency', array($currency));

        return parent::setCurrency($currency);
    }

    /**
     * {@inheritDoc}
     */
    public function getPackagingUnit()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPackagingUnit', array());

        return parent::getPackagingUnit();
    }

    /**
     * {@inheritDoc}
     */
    public function setPackagingUnit($packagingUnit)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPackagingUnit', array($packagingUnit));

        return parent::setPackagingUnit($packagingUnit);
    }

    /**
     * {@inheritDoc}
     */
    public function getPart()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPart', array());

        return parent::getPart();
    }

    /**
     * {@inheritDoc}
     */
    public function setPart(\PartKeepr\PartBundle\Entity\Part $part = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPart', array($part));

        return parent::setPart($part);
    }

    /**
     * {@inheritDoc}
     */
    public function getDistributor()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDistributor', array());

        return parent::getDistributor();
    }

    /**
     * {@inheritDoc}
     */
    public function setDistributor(\PartKeepr\DistributorBundle\Entity\Distributor $distributor)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDistributor', array($distributor));

        return parent::setDistributor($distributor);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrderNumber()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOrderNumber', array());

        return parent::getOrderNumber();
    }

    /**
     * {@inheritDoc}
     */
    public function setOrderNumber($orderNumber)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOrderNumber', array($orderNumber));

        return parent::setOrderNumber($orderNumber);
    }

    /**
     * {@inheritDoc}
     */
    public function getPrice()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPrice', array());

        return parent::getPrice();
    }

    /**
     * {@inheritDoc}
     */
    public function setPrice($price)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPrice', array($price));

        return parent::setPrice($price);
    }

    /**
     * {@inheritDoc}
     */
    public function getSku()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSku', array());

        return parent::getSku();
    }

    /**
     * {@inheritDoc}
     */
    public function setSku($sku)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSku', array($sku));

        return parent::setSku($sku);
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', array());

        return parent::__toString();
    }

}
