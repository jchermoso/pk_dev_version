<?php

namespace Proxies\__CG__\PartKeepr\ProjectBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ProjectPart extends \PartKeepr\ProjectBundle\Entity\ProjectPart implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'part', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'quantity', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'project', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'remarks', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'overageType', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'overage', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'lotNumber', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'totalQuantity');
        }

        return array('__isInitialized__', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'part', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'quantity', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'project', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'remarks', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'overageType', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'overage', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'lotNumber', '' . "\0" . 'PartKeepr\\ProjectBundle\\Entity\\ProjectPart' . "\0" . 'totalQuantity');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ProjectPart $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getTotalQuantity()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTotalQuantity', array());

        return parent::getTotalQuantity();
    }

    /**
     * {@inheritDoc}
     */
    public function getLotNumber()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLotNumber', array());

        return parent::getLotNumber();
    }

    /**
     * {@inheritDoc}
     */
    public function setLotNumber($lotNumber)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLotNumber', array($lotNumber));

        return parent::setLotNumber($lotNumber);
    }

    /**
     * {@inheritDoc}
     */
    public function getOverageType()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOverageType', array());

        return parent::getOverageType();
    }

    /**
     * {@inheritDoc}
     */
    public function setOverageType($overageType)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOverageType', array($overageType));

        return parent::setOverageType($overageType);
    }

    /**
     * {@inheritDoc}
     */
    public function getOverage()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOverage', array());

        return parent::getOverage();
    }

    /**
     * {@inheritDoc}
     */
    public function setOverage($overage)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOverage', array($overage));

        return parent::setOverage($overage);
    }

    /**
     * {@inheritDoc}
     */
    public function getPart()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPart', array());

        return parent::getPart();
    }

    /**
     * {@inheritDoc}
     */
    public function setPart(\PartKeepr\PartBundle\Entity\Part $part)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPart', array($part));

        return parent::setPart($part);
    }

    /**
     * {@inheritDoc}
     */
    public function getQuantity()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getQuantity', array());

        return parent::getQuantity();
    }

    /**
     * {@inheritDoc}
     */
    public function setQuantity($quantity)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setQuantity', array($quantity));

        return parent::setQuantity($quantity);
    }

    /**
     * {@inheritDoc}
     */
    public function getProject()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProject', array());

        return parent::getProject();
    }

    /**
     * {@inheritDoc}
     */
    public function setProject(\PartKeepr\ProjectBundle\Entity\Project $project = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProject', array($project));

        return parent::setProject($project);
    }

    /**
     * {@inheritDoc}
     */
    public function getRemarks()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRemarks', array());

        return parent::getRemarks();
    }

    /**
     * {@inheritDoc}
     */
    public function setRemarks($remarks)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRemarks', array($remarks));

        return parent::setRemarks($remarks);
    }

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', array());

        return parent::__toString();
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

}
