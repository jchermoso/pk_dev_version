Ext.define('PartKeepr.FootprintBundle.Entity.Footprint', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.FootprintBundle.Entity.Footprint',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []}
                            ,
                            { name: 'category',
                reference: 'PartKeepr.FootprintBundle.Entity.FootprintCategory',
            allowBlank: true                        }
                                        ,
                            { name: 'image',
                reference: 'PartKeepr.FootprintBundle.Entity.FootprintImage'
                }
                    
    ],

        hasMany: [
            {
        name: 'attachments',
        associationKey: 'attachments',
        model: 'PartKeepr.FootprintBundle.Entity.FootprintAttachment'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/footprints'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.FootprintBundle.Entity.Footprint', '');
