Ext.define('PartKeepr.ManufacturerBundle.Entity.Manufacturer', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ManufacturerBundle.Entity.Manufacturer',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'address', type: 'string', allowNull: true, validators: []},
                { name: 'url', type: 'string', allowNull: true, validators: []},
                { name: 'email', type: 'string', allowNull: true, validators: []},
                { name: 'comment', type: 'string', allowNull: true, validators: []},
                { name: 'phone', type: 'string', allowNull: true, validators: []},
                { name: 'fax', type: 'string', allowNull: true, validators: []}
                        
    ],

        hasMany: [
            {
        name: 'icLogos',
        associationKey: 'icLogos',
        model: 'PartKeepr.ManufacturerBundle.Entity.ManufacturerICLogo'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/manufacturers'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ManufacturerBundle.Entity.Manufacturer', '');
