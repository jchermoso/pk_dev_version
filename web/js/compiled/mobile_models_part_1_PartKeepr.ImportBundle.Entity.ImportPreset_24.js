Ext.define('PartKeepr.ImportBundle.Entity.ImportPreset', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ImportBundle.Entity.ImportPreset',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'baseEntity', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'configuration', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/import_presets'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ImportBundle.Entity.ImportPreset', '');
