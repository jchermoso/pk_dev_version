Ext.define('PartKeepr.PartBundle.Entity.PartDistributor', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartDistributor',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'orderNumber', type: 'string', allowNull: true, validators: []},
                { name: 'packagingUnit', type: 'int', validators: []},
                { name: 'price', type: 'number', allowNull: true, validators: []},
                { name: 'currency', type: 'string', allowNull: true, validators: []},
                { name: 'sku', type: 'string', allowNull: true, validators: []},
                { name: 'ignoreForReports', type: 'boolean', allowNull: true, validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'distributor',
                reference: 'PartKeepr.DistributorBundle.Entity.Distributor',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.PartBundle.Entity.PartDistributor'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartDistributor', '');
