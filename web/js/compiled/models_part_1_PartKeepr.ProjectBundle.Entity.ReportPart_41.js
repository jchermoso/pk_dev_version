Ext.define('PartKeepr.ProjectBundle.Entity.ReportPart', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ReportPart',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'quantity', type: 'int', validators: []}
                            ,
                            { name: 'report',
                reference: 'PartKeepr.ProjectBundle.Entity.Report',
            allowBlank: true                        },
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'distributor',
                reference: 'PartKeepr.DistributorBundle.Entity.Distributor',
            allowBlank: true                        }
                            
    ],

        hasMany: [
            {
        name: 'subParts',
        associationKey: 'subParts',
        model: 'PartKeepr.PartBundle.Entity.Part'
        },
            {
        name: 'projectParts',
        associationKey: 'projectParts',
        model: 'PartKeepr.ProjectBundle.Entity.ProjectPart'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/project_report_parts'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ReportPart', '');
