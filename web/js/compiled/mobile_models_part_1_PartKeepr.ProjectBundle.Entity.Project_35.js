Ext.define('PartKeepr.ProjectBundle.Entity.Project', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.Project',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []}
                            ,
                            { name: 'user',
                reference: 'PartKeepr.AuthBundle.Entity.User',
            allowBlank: true                        }
                            
    ],

        hasMany: [
            {
        name: 'parts',
        associationKey: 'parts',
        model: 'PartKeepr.ProjectBundle.Entity.ProjectPart'
        },
            {
        name: 'attachments',
        associationKey: 'attachments',
        model: 'PartKeepr.ProjectBundle.Entity.ProjectAttachment'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/projects'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.Project', '');
