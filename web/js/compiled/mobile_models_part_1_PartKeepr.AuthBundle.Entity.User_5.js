Ext.define('PartKeepr.AuthBundle.Entity.User', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.AuthBundle.Entity.User',

    idProperty: "@id",
    fields: [
                { name: 'newPassword', type: 'string'},
                { name: 'initialUserPreferences', type: 'string'},
                { name: '@id', type: 'string', validators: []},
                { name: 'username', type: 'string', validators: []},
                { name: 'password', type: 'string', allowNull: true, validators: []},
                { name: 'email', type: 'string', allowNull: true, validators: []},
                { name: 'admin', type: 'boolean', validators: []},
                { name: 'legacy', type: 'boolean', validators: []},
                { name: 'lastSeen', type: 'date', allowNull: true, validators: []},
                { name: 'active', type: 'boolean', validators: []},
                { name: 'protected', type: 'boolean', validators: []}
                            ,
                            { name: 'provider',
                reference: 'PartKeepr.AuthBundle.Entity.UserProvider',
            allowBlank: true                        }
                            
    ],

        hasMany: [
            {
        name: 'tipHistories',
        associationKey: 'tipHistories',
        model: 'PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDayHistory'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/users'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.AuthBundle.Entity.User', '');
