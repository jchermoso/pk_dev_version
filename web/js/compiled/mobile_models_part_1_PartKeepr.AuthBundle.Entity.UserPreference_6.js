Ext.define('PartKeepr.AuthBundle.Entity.UserPreference', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.AuthBundle.Entity.UserPreference',

    idProperty: "@id",
    fields: [
                { name: 'preferenceKey', type: 'string', validators: []},
                { name: 'preferenceValue', type: 'string', validators: []}
                            ,
                            { name: 'user',
                reference: 'PartKeepr.AuthBundle.Entity.User',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/user_preferences'
                , ignoreIds: true
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.AuthBundle.Entity.UserPreference', '');
