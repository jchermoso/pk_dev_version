Ext.define("PartKeepr.data.CallActions", {
    /**
     * Calls an action relative to the entity.
     *
     * For example, if the entity has a method called "setDefault" and your ID is
     * "/PartKeepr/web/app_dev.php/api/part_measurement_units/1", callAction would call
     * "/PartKeepr/web/app_dev.php/api/part_measurement_units/1/setDefault" as a result.
     *
     * @param {String} action The action name
     * @param {Object} parameters (optional) The parameters as JS object
     * @param {Function} callback (optional) A callback function, or null if not required
     * @param {boolean} reload (optional) Triggers a reload of the model after executing the action
     */
    callPutAction: function (action, parameters, callback, reload)
    {
        var proxy = this.getProxy();

        proxy.callAction(this, action, "PUT", parameters, callback, reload);
    },
    /**
     * Calls an action relative to the entity.
     *
     * For example, if the entity has a method called "setDefault" and your ID is
     * "/PartKeepr/web/app_dev.php/api/part_measurement_units/1", callAction would call
     * "/PartKeepr/web/app_dev.php/api/part_measurement_units/1/setDefault" as a result.
     *
     * @param {String} action The action name
     * @param {Object} parameters (optional) The parameters as JS object
     * @param {Function} callback (optional) A callback function, or null if not required
     * @param {boolean} reload (optional) Triggers a reload of the model after executing the action
     */
    callGetAction: function (action, parameters, callback, reload)
    {
        var proxy = this.getProxy();

        proxy.callAction(this, action, "GET", parameters, callback, reload);
    },
    /**
     * Calls an action relative to the entity.
     *
     * For example, if the entity has a method called "setDefault" and your ID is
     * "/PartKeepr/web/app_dev.php/api/part_measurement_units/1", callAction would call
     * "/PartKeepr/web/app_dev.php/api/part_measurement_units/1/setDefault" as a result.
     *
     * @param {String} action The action name
     * @param {Object} parameters (optional) The parameters as JS object
     * @param {Function} callback (optional) A callback function, or null if not required
     * @param {boolean} reload (optional) Triggers a reload of the model after executing the action
     */
    callDeleteAction: function (action, parameters, callback, reload)
    {
        var proxy = this.getProxy();

        proxy.callAction(this, action, "DELETE", parameters, callback, reload);
    },
    getData: function (options)
    {
        var data = this.callParent(options);

        if (this.phantom) {
            delete data[this.idProperty];
        }

        return data;
    },
    /**
     * Returns data from all associations
     *
     * @return {Object} An object containing the associations as properties
     */
    getAssociationData: function ()
    {
        var roleName, values = [], role, item, store;

        for (roleName in this.associations) {
            role = this.associations[roleName];
            item = role.getAssociatedItem(this);
            if (!item || item.$gathering) {
                continue;
            }

            var getterName = this.associations[roleName].getterName;

            if (item.isStore) {
                store = this[getterName]();
                values[roleName] = store.getData().items;
            } else {
                values[roleName] = this[getterName]();
            }
        }

        return values;
    },
    /**
     * Sets data to all associations
     *
     * @param {Object} data The associations to set. Silently ignores non-existant associations.
     */
    setAssociationData: function (data)
    {
        var setterName, getterName, roleName, store;

        for (roleName in data) {
            if (this.associations[roleName]) {

                if (this.associations[roleName].isMany === true) {
                    getterName = this.associations[roleName].getterName;
                    store = this[getterName]();
                    store.add(data[roleName]);
                } else {
                    setterName = this.associations[roleName].setterName;
                    this[setterName](data[roleName]);
                }
            }
        }
    },
    inheritableStatics: {
        callPostCollectionAction: function (action, parameters, callback, ignoreException)
        {
            var proxy = this.getProxy();

            proxy.callCollectionAction(action, "POST", parameters, callback, ignoreException);
        },
        callGetCollectionAction: function (action, parameters, callback, ignoreException)
        {
            var proxy = this.getProxy();

            proxy.callCollectionAction(action, "GET", parameters, callback, ignoreException);
        }
    }
});

Ext.define('PartKeepr.data.field.Array', {
    extend: 'Ext.data.field.Field',

    alias: [
        'data.field.array'
    ],

    /**
     * @property [trueRe]
     * Values matching this regular expression are considered `true`.
     */
    trueRe: /^\s*(?:true|yes|on|1)\s*$/i,

    convert: function (v) {
        return v;
    },

    getType: function() {
        return 'array';
    }
});
Ext.define("PartKeepr.data.HydraModel", {
    extend: 'Ext.data.Model',

    mixins: ['PartKeepr.data.CallActions'],

    getData: function ()
    {
        var data = this.callParent(arguments);

        if (this.phantom) {
            delete data[this.idProperty];
        }

        return data;
    },
    isPartiallyEqualTo: function (model, fields)
    {
        var i;

        for (i = 0; i < fields.length; i++) {
            if (this.get(fields[i]) != model.get(fields[i])) {
                return false;
            }
        }

        return true;
    },
    get: function (fieldName)
    {
        var ret, role, item, openingBracket, closingBracket, subEntity, index, subEntityStore;

        ret = this.callParent(arguments);

        if (ret === undefined) {
            // The field is undefined, attempt to retrieve data via associations

            if (typeof(fieldName) !== "string") {
                return undefined;
            }

            var parts = fieldName.split(".");

            if (parts.length < 2) {
                return ret;
            }

            if (this.associations[parts[0]] && this.getFieldType(parts[0]).type === "onetomany") {
                role = this.associations[parts[0]];
                item = role.getAssociatedItem(this);

                if (item !== null) {
                    parts.shift();
                    return item.get(parts.join("."));
                }
            } else {
                openingBracket = parts[0].indexOf("[");

                if (openingBracket !== -1) {
                    subEntity = parts[0].substring(0, openingBracket);
                    closingBracket = parts[0].indexOf("]", openingBracket);
                    index = parts[0].substring(openingBracket+1, closingBracket);
                }
                else {
                    // No index was passed for retrieving this field, try to return the first array member
                    subEntity = parts[0];
                    index = 0;    
                }

                subEntityStore = this[this.associations[subEntity].role]();
                item = subEntityStore.getAt(index);

                if (item !== null) {
                    parts.shift();
                    return item.get(parts.join("."));
                }
            }
        }
        return ret;
    },
    /**
     * Returns the field type for a given field path as an object with the following properties:
     *
     * {
     *  type: "field", "onetomany" or "manytoone"
     *  reference: Only set if the type is "onetomany" or "manytoone" - holds the class name for the relation
     * }
     */
    getFieldType: function (fieldName)
    {
        var ret = null, role, tmp, i;

        for (i=0;i<this.fields.length;i++) {
            if (this.fields[i].getName() === fieldName) {
                if (this.fields[i].reference !== null) {
                    return {
                        type: "onetomany",
                        reference: this.fields[i].reference
                    };
                } else {
                    ret = {
                        type: "field"
                    };
                }
            }
        }

        if (this.associations[fieldName]) {
            return {
                type: "manytoone",
                reference: this.associations[fieldName].type
            };
        }

        if (ret === null) {
            // The field is undefined, attempt to retrieve data via associations
            var parts = fieldName.split(".");

            if (parts.length < 2) {
                return null;
            }

            for (i=0;i<this.fields.length;i++) {
                if (this.fields[i].getName() === parts[0]) {
                    parts.shift();
                    tmp = Ext.create(this.fields[i].reference.type);
                    return tmp.getFieldType(parts.join("."));
                }
            }

            if (this.associations[parts[0]]) {
                role = this.associations[parts[0]];
                tmp = Ext.create(role.type);
                parts.shift();
                return tmp.getFieldType(parts.join("."));
            }
        }
        return ret;
    },
    /**
     * Gets all of the data from this Models *loaded* associations. It does this
     * recursively. For example if we have a User which hasMany Orders, and each Order
     * hasMany OrderItems, it will return an object like this:
     *
     *     {
     *         orders: [
     *             {
     *                 id: 123,
     *                 status: 'shipped',
     *                 orderItems: [
     *                     ...
     *                 ]
     *             }
     *         ]
     *     }
     *
     * @param {Object} [result] The object on to which the associations will be added. If
     * no object is passed one is created. This object is then returned.
     * @param {Boolean/Object} [options] An object containing options describing the data
     * desired.
     * @param {Boolean} [options.associated=true] Pass `true` to include associated data from
     * other associated records.
     * @param {Boolean} [options.changes=false] Pass `true` to only include fields that
     * have been modified. Note that field modifications are only tracked for fields that
     * are not declared with `persist` set to `false`. In other words, only persistent
     * fields have changes tracked so passing `true` for this means `options.persist` is
     * redundant.
     * @param {Boolean} [options.critical] Pass `true` to include fields set as `critical`.
     * This is only meaningful when `options.changes` is `true` since critical fields may
     * not have been modified.
     * @param {Boolean} [options.persist] Pass `true` to only return persistent fields.
     * This is implied when `options.changes` is set to `true`.
     * @param {Boolean} [options.serialize=false] Pass `true` to invoke the `serialize`
     * method on the returned fields.
     * @return {Object} The nested data set for the Model's loaded associations.
     */
    getAssociatedData: function (result, options)
    {
        var me = this,
            associations = me.associations,
            deep, i, item, items, itemData, length,
            record, role, roleName, opts, clear, associated;

        result = result || {};

        me.$gathering = 1;

        if (options) {
            options = Ext.Object.chain(options);
        }

        for (roleName in associations) {
            role = associations[roleName];

            item = role.getAssociatedItem(me);
            if (!item || item.$gathering) {
                continue;
            }

            if (item.isStore) {
                item.$gathering = 1;

                items = item.getData().items; // get the records for the store
                length = items.length;
                itemData = [];

                for (i = 0; i < length; ++i) {
                    // NOTE - we don't check whether the record is gathering here because
                    // we cannot remove it from the store (it would invalidate the index
                    // values and misrepresent the content). Instead we tell getData to
                    // only get the fields vs descend further.
                    record = items[i];
                    deep = !record.$gathering;
                    record.$gathering = 1;
                    if (options) {
                        associated = options.associated;
                        if (associated === undefined) {
                            options.associated = deep;
                            clear = true;
                        } else {
                            if (!deep) {
                                options.associated = false;
                                clear = true;
                            }
                        }
                        opts = options;
                    } else {
                        opts = deep ? me._getAssociatedOptions : me._getNotAssociatedOptions;
                    }
                    itemData.push(record.getData(opts));
                    if (clear) {
                        options.associated = associated;
                        clear = false;
                    }
                    delete record.$gathering;
                }

                delete item.$gathering;
            } else {
                opts = options || me._getAssociatedOptions;
                if (options && options.associated === undefined) {
                    opts.associated = true;
                }

                if (this.getField(roleName) !== null && this.getField(roleName).byReference) {
                    itemData = item.getId();
                } else {
                    itemData = item.getData(opts);
                }
            }

            result[roleName] = itemData;
        }

        delete me.$gathering;

        return result;
    },

    /**
     * Returns data from all associations
     *
     * @return {Object} An object containing the associations as properties
     */
    getAssociationData: function ()
    {
        var roleName, values = [], role, item, store;

        for (roleName in this.associations) {
            role = this.associations[roleName];
            item = role.getAssociatedItem(this);
            if (!item || item.$gathering) {
                continue;
            }

            var getterName = this.associations[roleName].getterName;

            if (item.isStore) {
                store = this[getterName]();
                values[roleName] = store.getData().items;
            } else {
                values[roleName] = this[getterName]();
            }
        }

        return values;
    },
    /**
     * Sets data to all associations
     *
     * @param {Object} data The associations to set. Silently ignores non-existant associations.
     */
    setAssociationData: function (data)
    {
        var setterName, getterName, roleName, store, idProperty;

        for (roleName in data) {
            if (this.associations[roleName]) {

                if (this.associations[roleName].isMany === true) {
                    getterName = this.associations[roleName].getterName;
                    store = this[getterName]();

                    for (var i = 0; i < data[roleName].length; i++) {
                        // Delete existing IDs for duplicated data
                        if (data[roleName][i].isEntity) {
                            idProperty = data[roleName][i].idProperty;
                            delete data[roleName][i].data[idProperty];
                        }
                    }
                    store.add(data[roleName]);
                } else {
                    setterName = this.associations[roleName].setterName;
                    this[setterName](data[roleName]);
                }
            }
        }
    },
    inheritableStatics: {
        callPostCollectionAction: function (action, parameters, callback, ignoreException)
        {
            var proxy = this.getProxy();

            proxy.callCollectionAction(action, "POST", parameters, callback, ignoreException);
        },
        callGetCollectionAction: function (action, parameters, callback, ignoreException)
        {
            var proxy = this.getProxy();

            proxy.callCollectionAction(action, "GET", parameters, callback, ignoreException);
        }
    }
});

/**
 * Adds the config field "byReference" to a field.
 *
 * byReference tells the system not to serialize the whole item but only its reference.
 */
Ext.define('PartKeepr.data.HydraField', {
    override: "Ext.data.field.Field",

    byReference: false,

    constructor: function (config)
    {
        if (config.byReference) {
            this.byReference = config.byReference;
        } else {
            this.byReference = false;
        }
        this.callParent(arguments);
    }
});

Ext.define("PartKeepr.data.HydraTreeModel", {
    extend: 'Ext.data.TreeModel',

    mixins: ['PartKeepr.data.CallActions']
});

Ext.define("PartKeepr.Data.Store.ModelStore", {
    extend: "Ext.data.Store",

    storeId: "ModelStore",

    fields: [
        {
            name: "model", type: "string"
        }, {
            name: "description", type: "string"
        }
    ],

    statics: {
        addModel: function (model, description)
        {
            Ext.StoreManager.lookup("ModelStore").add({
                model: model,
                description: description
            });
        }
    }
});

Ext.create("PartKeepr.Data.Store.ModelStore");

Ext.define("PartKeepr.Data.Store.BaseStore", {
    extend: "Ext.data.Store",

    autoSync: false, // Do not change. If true, new (empty) records would be immediately committed to the database.
    remoteFilter: true,
    remoteSort: true,
    pageSize: 15,

    /**
     * @var {Boolean} Specifies if the search field has an active search
     */
    hasSearch: false,

    /**
     * @cfg {String} Specifies the target property to search
     */
    targetField: 'query',

    /**
     * @cfg {String} Specifies the system property which defines all fields to be searched
     */
    searchFieldSystemPreference: null,

    /**
     * @cfg {Array} Specifies the default fields to be searched
     */
    searchFieldSystemPreferenceDefaults: [],

    /**
     * @cfg {String} Specifies the system property which defines if the search terms should be splitted
     */
    splitSearchTermSystemPreference: null,

    /**
     * @cfg {String} Specifies the default for search term splitting
     */
    splitSearchTermSystemPreferenceDefaults: true,

    /**
     * @var {Ext.util.Filter} The filter set by the search field
     */
    searchFilter: null,

    listeners: {
        // Workaround for bug http://www.sencha.com/forum/showthread.php?133767-Store.sync()-does-not-update-dirty-flag&p=607093#post607093
        // TODO: Check if this is still present in ExtJS 6.x
        write: function (store, operation) {
            var success = operation.wasSuccessful();
            if (success) {
                Ext.each(operation.records, function (record)
                {
                    if (record.dirty) {
                        record.commit();
                    }
                });
            }
        }
    },

    constructor: function () {
        this.searchFilter = Ext.create("PartKeepr.util.Filter");
        this.callParent(arguments);
    },

    doSearch: function (searchValue) {
        if (typeof searchValue !== "string") {
            return;
        }

        var searchTerms = searchValue.split(" "),
            splitTerms = true,
            orSubFilters = [],
            i,
            j,
            subFilters = [];

        if (this.splitSearchTermSystemPreference !== null) {
            splitTerms = Boolean(PartKeepr.getApplication().getSystemPreference(this.splitSearchTermSystemPreference,
                this.splitSearchTermSystemPreferenceDefaults));
        }

        if (this.searchFieldSystemPreference !== null) {
            var fields = PartKeepr.getApplication().getSystemPreference(this.searchFieldSystemPreference,
                this.searchFieldSystemPreferenceDefaults);

            if (splitTerms === true) {
                for (j = 0; j < searchTerms.length; j++) {
                    orSubFilters = [];
                    for (i = 0; i < fields.length; i++) {
                        orSubFilters.push(this.createSearchFilter(fields[i], searchTerms[j]));
                    }

                    subFilters.push(Ext.create("PartKeepr.util.Filter", {
                        type: "OR",
                        subfilters: orSubFilters
                    }));
                }

                this.searchFilter.setConfig({
                    type: "AND",
                    subfilters: subFilters
                });

            } else {
                for (i = 0; i < fields.length; i++) {
                    subFilters.push(this.createSearchFilter(fields[i], searchValue));
                }

                this.searchFilter.setConfig({
                    type: "OR",
                    subfilters: subFilters
                });
            }


        } else {
            if (splitTerms === true) {
                for (j = 0; j < searchTerms.length; j++) {
                    subFilters.push(this.createSearchFilter(this.targetField, searchTerms[j]));
                }

                this.searchFilter.setConfig({
                    type: "OR",
                    subfilters: subFilters
                });
            } else {
                this.searchFilter.setConfig({
                    property: this.targetField,
                    value: "%" + searchValue+ "%",
                    operator: 'like'
                });
            }
        }

        if (searchValue.length < 1) {
            this.resetSearch();
            return;
        }

        if (this.isLoading()) {
            Ext.defer(this.startSearch, 200, this);
            return;
        }

        this.searchFilter.setValue(searchValue);

        if (!this.getFilters().contains(this.searchFilter)) {
            this.getFilters().add(this.searchFilter);
        }

        this.getFilters().itemChanged(this.searchFilter);

        this.hasSearch = true;
    },

    createSearchFilter: function (property, term) {
        return Ext.create("PartKeepr.util.Filter", {
            property: property,
            value: "%" + term + "%",
            operator: 'like'
        });
    },
    /**
     * Resets the search field to empty and re-triggers the store to load the matching records.
     */
    resetSearch: function ()
    {
        if (this.isLoading()) {
            Ext.defer(this.resetSearch, 200, this);
            return;
        }

        this.searchFilter.setValue('');

        if (this.hasSearch) {

            if (this.getFilters().contains(this.searchFilter)) {
                this.getFilters().remove(this.searchFilter);
            }

            this.currentPage = 1;
            this.load({start: 0});
            this.hasSearch = false;

        }
    }


});
//@todo remove with ExtJS 6.2.1
Ext.define('Ext.overrides.form.field.Checkbox', {
    override : 'Ext.form.field.Checkbox',

    uncheckedValue : false
});

/**
 * Overrides the renderCell method so it uses the record.get() call and not the dataIndex on the record's data
 * array itself. This allows to use nested queries like storageLocation.name
 */
Ext.define("PartKeepr.ExtJS.Enhancements.renderCell", {
    override: 'Ext.view.Table',

    /**
     * @private
     * Emits the HTML representing a single grid cell into the passed output stream (which is an array of strings).
     *
     * @param {Ext.grid.column.Column} column The column definition for which to render a cell.
     * @param {Ext.data.Model} record The record being edited
     * @param {Number} recordIndex The row index (zero based within the {@link #store}) for which to render the cell.
     * @param {Number} rowIndex The row index (zero based within this view for which to render the cell.
     * @param {Number} columnIndex The column index (zero based) for which to render the cell.
     * @param {String[]} out The output stream into which the HTML strings are appended.
     */
    renderCell: function (column, record, recordIndex, rowIndex, columnIndex, out) {
        var me = this,
            fullIndex,
            selModel = me.selectionModel,
            cellValues = me.cellValues,
            classes = cellValues.classes,
            fieldValue,
            cellTpl = me.cellTpl,
            enableTextSelection = column.enableTextSelection,
            value, clsInsertPoint,
            lastFocused = me.navigationModel.getPosition();

        fieldValue = record.get(column.dataIndex);

        // Only use the view's setting if it's not been overridden on the column
        if (enableTextSelection == null) {
            enableTextSelection = me.enableTextSelection;
        }

        cellValues.record = record;
        cellValues.column = column;
        cellValues.recordIndex = recordIndex;
        cellValues.rowIndex = rowIndex;
        cellValues.columnIndex = cellValues.cellIndex = columnIndex;
        cellValues.align = column.textAlign;
        cellValues.innerCls = column.innerCls;
        cellValues.tdCls = cellValues.tdStyle = cellValues.tdAttr = cellValues.style = "";
        cellValues.unselectableAttr = enableTextSelection ? '' : 'unselectable="on"';

        // Begin setup of classes to add to cell
        classes[1] = column.getCellId();

        // On IE8, array[len] = 'foo' is twice as fast as array.push('foo')
        // So keep an insertion point and use assignment to help IE!
        clsInsertPoint = 2;

        if (column.renderer && column.renderer.call) {
            fullIndex = me.ownerCt.columnManager.getHeaderIndex(column);
            value = column.renderer.call(column.usingDefaultRenderer ? column : column.scope || me.ownerCt, fieldValue, cellValues, record, recordIndex, fullIndex, me.dataSource, me);
            if (cellValues.css) {
                // This warning attribute is used by the compat layer
                // TODO: remove when compat layer becomes deprecated
                record.cssWarning = true;
                cellValues.tdCls += ' ' + cellValues.css;
                cellValues.css = null;
            }

            // Add any tdCls which was added to the cellValues by the renderer.
            if (cellValues.tdCls) {
                classes[clsInsertPoint++] = cellValues.tdCls;
            }
        } else {
            value = fieldValue;
        }

        cellValues.value = (value == null || value.length === 0) ? column.emptyCellText : value;

        if (column.tdCls) {
            classes[clsInsertPoint++] = column.tdCls;
        }
        if (me.markDirty && record.dirty && record.isModified(column.dataIndex)) {
            classes[clsInsertPoint++] = me.dirtyCls;

            if (column.dirtyTextElementId) {
                cellValues.tdAttr = (cellValues.tdAttr ? cellValues.tdAttr + ' ' : '') +
                                    'aria-describedby="' + column.dirtyTextElementId + '"';
            }
        }
        if (column.isFirstVisible) {
            classes[clsInsertPoint++] = me.firstCls;
        }
        if (column.isLastVisible) {
            classes[clsInsertPoint++] = me.lastCls;
        }
        if (!enableTextSelection) {
            classes[clsInsertPoint++] = me.unselectableCls;
        }
        if (selModel && (selModel.isCellModel || selModel.isSpreadsheetModel) && selModel.isCellSelected(me, recordIndex, column)) {
            classes[clsInsertPoint++] = me.selectedCellCls;
        }
        if (lastFocused && lastFocused.record.id === record.id && lastFocused.column === column) {
            classes[clsInsertPoint++] = me.focusedItemCls;
        }

        // Chop back array to only what we've set
        classes.length = clsInsertPoint;

        cellValues.tdCls = classes.join(' ');

        cellTpl.applyOut(cellValues, out);

        // Dereference objects since cellValues is a persistent var in the XTemplate's scope chain
        cellValues.column = cellValues.record = null;
    }
});

/**
 * Overrides the Date field to return the date in ISO 8601 format by default
 */
Ext.define("PartKeepr.data.field.Date", {
    override: "Ext.data.field.Date",

    serialize: function (value)
    {
        if (value instanceof Date) {
            return value.toISOString();
        } else {
            var date = new Date();
            return date.toISOString();
        }
    }
});
