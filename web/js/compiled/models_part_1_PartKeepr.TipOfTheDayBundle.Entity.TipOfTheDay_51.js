Ext.define('PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDay', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDay',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/tip_of_the_days'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDay', '');
