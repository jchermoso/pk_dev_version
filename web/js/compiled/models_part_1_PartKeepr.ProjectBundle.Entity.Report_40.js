Ext.define('PartKeepr.ProjectBundle.Entity.Report', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.Report',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', allowNull: true, validators: []},
                { name: 'createDateTime', type: 'date', validators: []}
                        
    ],

        hasMany: [
            {
        name: 'reportProjects',
        associationKey: 'reportProjects',
        model: 'PartKeepr.ProjectBundle.Entity.ReportProject'
        },
            {
        name: 'reportParts',
        associationKey: 'reportParts',
        model: 'PartKeepr.ProjectBundle.Entity.ReportPart'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/reports'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.Report', '');
