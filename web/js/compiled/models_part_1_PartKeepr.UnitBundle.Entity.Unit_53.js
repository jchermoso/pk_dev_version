Ext.define('PartKeepr.UnitBundle.Entity.Unit', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.UnitBundle.Entity.Unit',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: [{"type":"presence","message":"unit.name.not_blank"}]},
                { name: 'symbol', type: 'string', validators: [{"type":"presence","message":"unit.symbol.not_blank"}]}
                        
    ],

    
        manyToMany: {
            prefixes: {
            type: 'PartKeepr.SiPrefixBundle.Entity.SiPrefix',
            role: 'prefixes',
            field: '@id',
            right: true
        }         },
    
    proxy: {
        type: "Hydra",
        url: '/api/units'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.UnitBundle.Entity.Unit', '');
