Ext.define('PartKeepr.ProjectBundle.Entity.ReportProject', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ReportProject',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'quantity', type: 'int', validators: []}
                            ,
                            { name: 'report',
                reference: 'PartKeepr.ProjectBundle.Entity.Report',
            allowBlank: true                        },
                            { name: 'project',
                reference: 'PartKeepr.ProjectBundle.Entity.Project',
            allowBlank: false                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/project_report_projects'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ReportProject', '');
