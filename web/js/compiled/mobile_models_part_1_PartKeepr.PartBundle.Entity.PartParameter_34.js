Ext.define('PartKeepr.PartBundle.Entity.PartParameter', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartParameter',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', validators: []},
                { name: 'value', type: 'number', allowNull: true, validators: []},
                { name: 'normalizedValue', type: 'number', allowNull: true, validators: []},
                { name: 'maxValue', type: 'number', allowNull: true, validators: []},
                { name: 'normalizedMaxValue', type: 'number', allowNull: true, validators: []},
                { name: 'minValue', type: 'number', allowNull: true, validators: []},
                { name: 'normalizedMinValue', type: 'number', allowNull: true, validators: []},
                { name: 'stringValue', type: 'string', validators: []},
                { name: 'valueType', type: 'string', validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'unit',
                reference: 'PartKeepr.UnitBundle.Entity.Unit',
            allowBlank: true                        },
                            { name: 'siPrefix',
                reference: 'PartKeepr.SiPrefixBundle.Entity.SiPrefix',
            allowBlank: true                        },
                            { name: 'minSiPrefix',
                reference: 'PartKeepr.SiPrefixBundle.Entity.SiPrefix',
            allowBlank: true                        },
                            { name: 'maxSiPrefix',
                reference: 'PartKeepr.SiPrefixBundle.Entity.SiPrefix',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.PartBundle.Entity.PartParameter'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartParameter', '');
