Ext.define('PartKeepr.PartBundle.Entity.PartManufacturer', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartManufacturer',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'partNumber', type: 'string', allowNull: true, validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'manufacturer',
                reference: 'PartKeepr.ManufacturerBundle.Entity.Manufacturer',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.PartBundle.Entity.PartManufacturer'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartManufacturer', '');
