Ext.define('PartKeepr.PartBundle.Entity.PartAttachment', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartAttachment',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []},
                { name: 'isImage', type: 'boolean', allowNull: true, validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.PartBundle.Entity.PartAttachment'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartAttachment', '');
