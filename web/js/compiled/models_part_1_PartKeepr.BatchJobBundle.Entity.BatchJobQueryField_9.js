Ext.define('PartKeepr.BatchJobBundle.Entity.BatchJobQueryField', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.BatchJobBundle.Entity.BatchJobQueryField',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'property', type: 'string', validators: []},
                { name: 'operator', type: 'string', validators: []},
                { name: 'value', type: 'string', validators: []},
                { name: 'description', type: 'string', validators: []},
                { name: 'dynamic', type: 'boolean', validators: []}
                            ,
                            { name: 'batchJob',
                reference: 'PartKeepr.BatchJobBundle.Entity.BatchJob',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.BatchJobBundle.Entity.BatchJobQueryField'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.BatchJobBundle.Entity.BatchJobQueryField', '');
