Ext.define('PartKeepr.StatisticBundle.Entity.StatisticSnapshotUnit', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.StatisticBundle.Entity.StatisticSnapshotUnit',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'stockLevel', type: 'int', validators: []}
                            ,
                            { name: 'statisticSnapshot',
                reference: 'PartKeepr.StatisticBundle.Entity.StatisticSnapshot',
            allowBlank: true                        },
                            { name: 'partUnit',
                reference: 'PartKeepr.PartBundle.Entity.PartMeasurementUnit',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.StatisticBundle.Entity.StatisticSnapshotUnit'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StatisticBundle.Entity.StatisticSnapshotUnit', '');
