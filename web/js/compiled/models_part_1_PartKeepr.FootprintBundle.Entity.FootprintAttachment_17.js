Ext.define('PartKeepr.FootprintBundle.Entity.FootprintAttachment', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.FootprintBundle.Entity.FootprintAttachment',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                            ,
                            { name: 'footprint',
                reference: 'PartKeepr.FootprintBundle.Entity.Footprint',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.FootprintBundle.Entity.FootprintAttachment'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.FootprintBundle.Entity.FootprintAttachment', '');
