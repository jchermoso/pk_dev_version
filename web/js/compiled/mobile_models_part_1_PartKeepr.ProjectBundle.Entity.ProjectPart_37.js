Ext.define('PartKeepr.ProjectBundle.Entity.ProjectPart', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ProjectPart',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'quantity', type: 'int', validators: []},
                { name: 'remarks', type: 'string', allowNull: true, validators: []},
                { name: 'overageType', type: 'string', validators: []},
                { name: 'overage', type: 'int', validators: []},
                { name: 'lotNumber', type: 'string', validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: false        ,byReference: true                },
                            { name: 'project',
                reference: 'PartKeepr.ProjectBundle.Entity.Project',
            allowBlank: false                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/project_parts'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ProjectPart', '');
