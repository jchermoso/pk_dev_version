Ext.define('PartKeepr.ProjectBundle.Entity.ProjectRun', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ProjectRun',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'runDateTime', type: 'date', validators: []},
                { name: 'quantity', type: 'int', validators: []}
                            ,
                            { name: 'project',
                reference: 'PartKeepr.ProjectBundle.Entity.Project',
            allowBlank: true                        }
                            
    ],

        hasMany: [
            {
        name: 'parts',
        associationKey: 'parts',
        model: 'PartKeepr.ProjectBundle.Entity.ProjectRunPart'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/project_runs'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ProjectRun', '');
