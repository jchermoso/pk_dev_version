Ext.define('PartKeepr.FrontendBundle.Entity.GridPreset', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.FrontendBundle.Entity.GridPreset',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'grid', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'configuration', type: 'string', validators: []},
                { name: 'gridDefault', type: 'boolean', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/grid_presets'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.FrontendBundle.Entity.GridPreset', '');
