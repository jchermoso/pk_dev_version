Ext.define('PartKeepr.StorageLocationBundle.Entity.StorageLocationImage', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.StorageLocationBundle.Entity.StorageLocationImage',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                                    ,
                            { name: 'storageLocation',
                reference: 'PartKeepr.StorageLocationBundle.Entity.StorageLocation'
                }
                    
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.StorageLocationBundle.Entity.StorageLocationImage'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StorageLocationBundle.Entity.StorageLocationImage', '');
