Ext.define('PartKeepr.BatchJobBundle.Entity.BatchJobUpdateField', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.BatchJobBundle.Entity.BatchJobUpdateField',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'property', type: 'string', validators: []},
                { name: 'value', type: 'string', validators: []},
                { name: 'description', type: 'string', validators: []},
                { name: 'dynamic', type: 'boolean', validators: []}
                            ,
                            { name: 'batchJob',
                reference: 'PartKeepr.BatchJobBundle.Entity.BatchJob',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.BatchJobBundle.Entity.BatchJobUpdateField'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.BatchJobBundle.Entity.BatchJobUpdateField', '');
