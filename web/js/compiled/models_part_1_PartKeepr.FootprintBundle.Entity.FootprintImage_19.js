Ext.define('PartKeepr.FootprintBundle.Entity.FootprintImage', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.FootprintBundle.Entity.FootprintImage',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                                    ,
                            { name: 'footprint',
                reference: 'PartKeepr.FootprintBundle.Entity.Footprint'
                }
                    
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.FootprintBundle.Entity.FootprintImage'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.FootprintBundle.Entity.FootprintImage', '');
