Ext.define('PartKeepr.ProjectBundle.Entity.ProjectRunPart', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ProjectRunPart',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'quantity', type: 'int', validators: []},
                { name: 'lotNumber', type: 'string', validators: []}
                            ,
                            { name: 'projectRun',
                reference: 'PartKeepr.ProjectBundle.Entity.ProjectRun',
            allowBlank: true                        },
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.ProjectBundle.Entity.ProjectRunPart'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ProjectRunPart', '');
