Ext.define('PartKeepr.UploadedFileBundle.Entity.UploadedFile', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.UploadedFileBundle.Entity.UploadedFile',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.UploadedFileBundle.Entity.UploadedFile'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.UploadedFileBundle.Entity.UploadedFile', '');
