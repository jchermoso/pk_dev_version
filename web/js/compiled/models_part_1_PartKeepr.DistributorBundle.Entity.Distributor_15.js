Ext.define('PartKeepr.DistributorBundle.Entity.Distributor', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.DistributorBundle.Entity.Distributor',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'address', type: 'string', allowNull: true, validators: []},
                { name: 'url', type: 'string', allowNull: true, validators: []},
                { name: 'phone', type: 'string', allowNull: true, validators: []},
                { name: 'fax', type: 'string', allowNull: true, validators: []},
                { name: 'email', type: 'string', allowNull: true, validators: []},
                { name: 'comment', type: 'string', allowNull: true, validators: []},
                { name: 'skuurl', type: 'string', allowNull: true, validators: []},
                { name: 'enabledForReports', type: 'boolean', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/distributors'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.DistributorBundle.Entity.Distributor', '');
