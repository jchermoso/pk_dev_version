Ext.define('PartKeepr.PartBundle.Entity.MetaPartParameterCriteria', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.MetaPartParameterCriteria',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'partParameterName', type: 'string', validators: []},
                { name: 'operator', type: 'string', validators: []},
                { name: 'value', type: 'number', allowNull: true, validators: []},
                { name: 'normalizedValue', type: 'number', allowNull: true, validators: []},
                { name: 'stringValue', type: 'string', validators: []},
                { name: 'valueType', type: 'string', validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'siPrefix',
                reference: 'PartKeepr.SiPrefixBundle.Entity.SiPrefix',
            allowBlank: true                        },
                            { name: 'unit',
                reference: 'PartKeepr.UnitBundle.Entity.Unit',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.PartBundle.Entity.MetaPartParameterCriteria'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.MetaPartParameterCriteria', '');
