Ext.define('PartKeepr.AuthBundle.Entity.FOSUser', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.AuthBundle.Entity.FOSUser',

    idProperty: "@id",
    fields: [
                { name: 'username', type: 'string', validators: []},
                { name: 'usernameCanonical', type: 'string', validators: []},
                { name: 'enabled', type: 'boolean', validators: []},
                { name: 'salt', type: 'string', allowNull: true, validators: []},
                { name: 'password', type: 'string', validators: []},
                { name: 'lastLogin', type: 'date', allowNull: true, validators: []},
                { name: 'confirmationToken', type: 'string', allowNull: true, validators: []},
                { name: 'passwordRequestedAt', type: 'date', allowNull: true, validators: []},
                { name: 'roles', type: 'array', validators: []},
                { name: '@id', type: 'string', validators: []},
                { name: 'emailCanonical', type: 'string', allowNull: true, validators: []},
                { name: 'email', type: 'string', allowNull: true, validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/f_o_s_users'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.AuthBundle.Entity.FOSUser', '');
