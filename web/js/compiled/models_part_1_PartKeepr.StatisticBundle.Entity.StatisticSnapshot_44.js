Ext.define('PartKeepr.StatisticBundle.Entity.StatisticSnapshot', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.StatisticBundle.Entity.StatisticSnapshot',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'dateTime', type: 'date', validators: []},
                { name: 'parts', type: 'int', validators: []},
                { name: 'categories', type: 'int', validators: []}
                        
    ],

        hasMany: [
            {
        name: 'units',
        associationKey: 'units',
        model: 'PartKeepr.StatisticBundle.Entity.StatisticSnapshotUnit'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.StatisticBundle.Entity.StatisticSnapshot'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StatisticBundle.Entity.StatisticSnapshot', '');
