Ext.define('PartKeepr.ManufacturerBundle.Entity.ManufacturerICLogo', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ManufacturerBundle.Entity.ManufacturerICLogo',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                            ,
                            { name: 'manufacturer',
                reference: 'PartKeepr.ManufacturerBundle.Entity.Manufacturer',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.ManufacturerBundle.Entity.ManufacturerICLogo'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ManufacturerBundle.Entity.ManufacturerICLogo', '');
