Ext.define('FOS.UserBundle.Model.User', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.FOS.UserBundle.Model.User',

    idProperty: "@id",
    fields: [
                { name: 'username', type: 'string', validators: []},
                { name: 'usernameCanonical', type: 'string', validators: []},
                { name: 'email', type: 'string', validators: []},
                { name: 'emailCanonical', type: 'string', validators: []},
                { name: 'enabled', type: 'boolean', validators: []},
                { name: 'salt', type: 'string', allowNull: true, validators: []},
                { name: 'password', type: 'string', validators: []},
                { name: 'lastLogin', type: 'date', allowNull: true, validators: []},
                { name: 'confirmationToken', type: 'string', allowNull: true, validators: []},
                { name: 'passwordRequestedAt', type: 'date', allowNull: true, validators: []},
                { name: 'roles', type: 'array', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:FOS.UserBundle.Model.User'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('FOS.UserBundle.Model.User', '');
