Ext.define('PartKeepr.CronLoggerBundle.Entity.CronLogger', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.CronLoggerBundle.Entity.CronLogger',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'lastRunDate', type: 'date', validators: []},
                { name: 'cronjob', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.CronLoggerBundle.Entity.CronLogger'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.CronLoggerBundle.Entity.CronLogger', '');
