Ext.define('PartKeepr.BatchJobBundle.Entity.BatchJob', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.BatchJobBundle.Entity.BatchJob',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'baseEntity', type: 'string', validators: []}
                        
    ],

        hasMany: [
            {
        name: 'batchJobQueryFields',
        associationKey: 'batchJobQueryFields',
        model: 'PartKeepr.BatchJobBundle.Entity.BatchJobQueryField'
        },
            {
        name: 'batchJobUpdateFields',
        associationKey: 'batchJobUpdateFields',
        model: 'PartKeepr.BatchJobBundle.Entity.BatchJobUpdateField'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/batch_jobs'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.BatchJobBundle.Entity.BatchJob', '');
