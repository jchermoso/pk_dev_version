Ext.define('PartKeepr.SystemPreferenceBundle.Entity.SystemPreference', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.SystemPreferenceBundle.Entity.SystemPreference',

    idProperty: "@id",
    fields: [
                { name: 'preferenceKey', type: 'string', validators: []},
                { name: 'preferenceValue', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/system_preferences'
                , ignoreIds: true
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.SystemPreferenceBundle.Entity.SystemPreference', '');
