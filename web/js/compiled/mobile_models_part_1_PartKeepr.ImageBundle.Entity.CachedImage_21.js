Ext.define('PartKeepr.ImageBundle.Entity.CachedImage', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ImageBundle.Entity.CachedImage',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'originalId', type: 'int', validators: []},
                { name: 'originalType', type: 'string', validators: []},
                { name: 'cacheFile', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.ImageBundle.Entity.CachedImage'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ImageBundle.Entity.CachedImage', '');
