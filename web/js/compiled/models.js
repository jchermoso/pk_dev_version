Ext.define('FOS.UserBundle.Model.Group', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.FOS.UserBundle.Model.Group',

    idProperty: "@id",
    fields: [
                { name: 'name', type: 'string', validators: []},
                { name: 'roles', type: 'array', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:FOS.UserBundle.Model.Group'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('FOS.UserBundle.Model.Group', '');

Ext.define('FOS.UserBundle.Model.User', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.FOS.UserBundle.Model.User',

    idProperty: "@id",
    fields: [
                { name: 'username', type: 'string', validators: []},
                { name: 'usernameCanonical', type: 'string', validators: []},
                { name: 'email', type: 'string', validators: []},
                { name: 'emailCanonical', type: 'string', validators: []},
                { name: 'enabled', type: 'boolean', validators: []},
                { name: 'salt', type: 'string', allowNull: true, validators: []},
                { name: 'password', type: 'string', validators: []},
                { name: 'lastLogin', type: 'date', allowNull: true, validators: []},
                { name: 'confirmationToken', type: 'string', allowNull: true, validators: []},
                { name: 'passwordRequestedAt', type: 'date', allowNull: true, validators: []},
                { name: 'roles', type: 'array', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:FOS.UserBundle.Model.User'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('FOS.UserBundle.Model.User', '');

Ext.define('Gedmo.Tree.Entity.MappedSuperclass.AbstractClosure', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.Gedmo.Tree.Entity.MappedSuperclass.AbstractClosure',

    idProperty: "@id",
    fields: [
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:Gedmo.Tree.Entity.MappedSuperclass.AbstractClosure'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('Gedmo.Tree.Entity.MappedSuperclass.AbstractClosure', '');

Ext.define('PartKeepr.AuthBundle.Entity.FOSUser', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.AuthBundle.Entity.FOSUser',

    idProperty: "@id",
    fields: [
                { name: 'username', type: 'string', validators: []},
                { name: 'usernameCanonical', type: 'string', validators: []},
                { name: 'enabled', type: 'boolean', validators: []},
                { name: 'salt', type: 'string', allowNull: true, validators: []},
                { name: 'password', type: 'string', validators: []},
                { name: 'lastLogin', type: 'date', allowNull: true, validators: []},
                { name: 'confirmationToken', type: 'string', allowNull: true, validators: []},
                { name: 'passwordRequestedAt', type: 'date', allowNull: true, validators: []},
                { name: 'roles', type: 'array', validators: []},
                { name: '@id', type: 'string', validators: []},
                { name: 'emailCanonical', type: 'string', allowNull: true, validators: []},
                { name: 'email', type: 'string', allowNull: true, validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/f_o_s_users'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.AuthBundle.Entity.FOSUser', '');

Ext.define('PartKeepr.AuthBundle.Entity.User', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.AuthBundle.Entity.User',

    idProperty: "@id",
    fields: [
                { name: 'newPassword', type: 'string'},
                { name: 'initialUserPreferences', type: 'string'},
                { name: '@id', type: 'string', validators: []},
                { name: 'username', type: 'string', validators: []},
                { name: 'password', type: 'string', allowNull: true, validators: []},
                { name: 'email', type: 'string', allowNull: true, validators: []},
                { name: 'admin', type: 'boolean', validators: []},
                { name: 'legacy', type: 'boolean', validators: []},
                { name: 'lastSeen', type: 'date', allowNull: true, validators: []},
                { name: 'active', type: 'boolean', validators: []},
                { name: 'protected', type: 'boolean', validators: []}
                            ,
                            { name: 'provider',
                reference: 'PartKeepr.AuthBundle.Entity.UserProvider',
            allowBlank: true                        }
                            
    ],

        hasMany: [
            {
        name: 'tipHistories',
        associationKey: 'tipHistories',
        model: 'PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDayHistory'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/users'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.AuthBundle.Entity.User', '');

Ext.define('PartKeepr.AuthBundle.Entity.UserPreference', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.AuthBundle.Entity.UserPreference',

    idProperty: "@id",
    fields: [
                { name: 'preferenceKey', type: 'string', validators: []},
                { name: 'preferenceValue', type: 'string', validators: []}
                            ,
                            { name: 'user',
                reference: 'PartKeepr.AuthBundle.Entity.User',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/user_preferences'
                , ignoreIds: true
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.AuthBundle.Entity.UserPreference', '');

Ext.define('PartKeepr.AuthBundle.Entity.UserProvider', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.AuthBundle.Entity.UserProvider',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'editable', type: 'boolean', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/user_providers'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.AuthBundle.Entity.UserProvider', '');

Ext.define('PartKeepr.BatchJobBundle.Entity.BatchJob', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.BatchJobBundle.Entity.BatchJob',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'baseEntity', type: 'string', validators: []}
                        
    ],

        hasMany: [
            {
        name: 'batchJobQueryFields',
        associationKey: 'batchJobQueryFields',
        model: 'PartKeepr.BatchJobBundle.Entity.BatchJobQueryField'
        },
            {
        name: 'batchJobUpdateFields',
        associationKey: 'batchJobUpdateFields',
        model: 'PartKeepr.BatchJobBundle.Entity.BatchJobUpdateField'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/batch_jobs'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.BatchJobBundle.Entity.BatchJob', '');

Ext.define('PartKeepr.BatchJobBundle.Entity.BatchJobQueryField', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.BatchJobBundle.Entity.BatchJobQueryField',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'property', type: 'string', validators: []},
                { name: 'operator', type: 'string', validators: []},
                { name: 'value', type: 'string', validators: []},
                { name: 'description', type: 'string', validators: []},
                { name: 'dynamic', type: 'boolean', validators: []}
                            ,
                            { name: 'batchJob',
                reference: 'PartKeepr.BatchJobBundle.Entity.BatchJob',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.BatchJobBundle.Entity.BatchJobQueryField'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.BatchJobBundle.Entity.BatchJobQueryField', '');

Ext.define('PartKeepr.BatchJobBundle.Entity.BatchJobUpdateField', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.BatchJobBundle.Entity.BatchJobUpdateField',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'property', type: 'string', validators: []},
                { name: 'value', type: 'string', validators: []},
                { name: 'description', type: 'string', validators: []},
                { name: 'dynamic', type: 'boolean', validators: []}
                            ,
                            { name: 'batchJob',
                reference: 'PartKeepr.BatchJobBundle.Entity.BatchJob',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.BatchJobBundle.Entity.BatchJobUpdateField'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.BatchJobBundle.Entity.BatchJobUpdateField', '');

Ext.define('PartKeepr.CategoryBundle.Entity.AbstractCategory', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.CategoryBundle.Entity.AbstractCategory',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'lft', type: 'int', validators: []},
                { name: 'rgt', type: 'int', validators: []},
                { name: 'lvl', type: 'int', validators: []},
                { name: 'root', type: 'int', allowNull: true, validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.CategoryBundle.Entity.AbstractCategory'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.CategoryBundle.Entity.AbstractCategory', '');

Ext.define('PartKeepr.CoreBundle.Entity.BaseEntity', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.CoreBundle.Entity.BaseEntity',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.CoreBundle.Entity.BaseEntity'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.CoreBundle.Entity.BaseEntity', '');

Ext.define('PartKeepr.CoreBundle.Entity.SystemNotice', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.CoreBundle.Entity.SystemNotice',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'date', type: 'date', validators: []},
                { name: 'title', type: 'string', validators: []},
                { name: 'description', type: 'string', validators: []},
                { name: 'acknowledged', type: 'boolean', validators: []},
                { name: 'type', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/system_notices'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.CoreBundle.Entity.SystemNotice', '');

Ext.define('PartKeepr.CronLoggerBundle.Entity.CronLogger', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.CronLoggerBundle.Entity.CronLogger',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'lastRunDate', type: 'date', validators: []},
                { name: 'cronjob', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.CronLoggerBundle.Entity.CronLogger'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.CronLoggerBundle.Entity.CronLogger', '');

Ext.define('PartKeepr.DistributorBundle.Entity.Distributor', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.DistributorBundle.Entity.Distributor',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'address', type: 'string', allowNull: true, validators: []},
                { name: 'url', type: 'string', allowNull: true, validators: []},
                { name: 'phone', type: 'string', allowNull: true, validators: []},
                { name: 'fax', type: 'string', allowNull: true, validators: []},
                { name: 'email', type: 'string', allowNull: true, validators: []},
                { name: 'comment', type: 'string', allowNull: true, validators: []},
                { name: 'skuurl', type: 'string', allowNull: true, validators: []},
                { name: 'enabledForReports', type: 'boolean', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/distributors'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.DistributorBundle.Entity.Distributor', '');

Ext.define('PartKeepr.FootprintBundle.Entity.Footprint', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.FootprintBundle.Entity.Footprint',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []}
                            ,
                            { name: 'category',
                reference: 'PartKeepr.FootprintBundle.Entity.FootprintCategory',
            allowBlank: true                        }
                                        ,
                            { name: 'image',
                reference: 'PartKeepr.FootprintBundle.Entity.FootprintImage'
                }
                    
    ],

        hasMany: [
            {
        name: 'attachments',
        associationKey: 'attachments',
        model: 'PartKeepr.FootprintBundle.Entity.FootprintAttachment'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/footprints'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.FootprintBundle.Entity.Footprint', '');

Ext.define('PartKeepr.FootprintBundle.Entity.FootprintAttachment', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.FootprintBundle.Entity.FootprintAttachment',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                            ,
                            { name: 'footprint',
                reference: 'PartKeepr.FootprintBundle.Entity.Footprint',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.FootprintBundle.Entity.FootprintAttachment'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.FootprintBundle.Entity.FootprintAttachment', '');

Ext.define('PartKeepr.FootprintBundle.Entity.FootprintCategory', {
    extend: 'PartKeepr.data.HydraTreeModel',
    alias: 'schema.PartKeepr.FootprintBundle.Entity.FootprintCategory',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'lft', type: 'int', validators: []},
                { name: 'rgt', type: 'int', validators: []},
                { name: 'lvl', type: 'int', validators: []},
                { name: 'root', type: 'int', allowNull: true, validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'categoryPath', type: 'string', allowNull: true, validators: []}
                        
    ],

        hasMany: [
            {
        name: 'footprints',
        associationKey: 'footprints',
        model: 'PartKeepr.FootprintBundle.Entity.Footprint'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/footprint_categories'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.FootprintBundle.Entity.FootprintCategory', '');

Ext.define('PartKeepr.FootprintBundle.Entity.FootprintImage', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.FootprintBundle.Entity.FootprintImage',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                                    ,
                            { name: 'footprint',
                reference: 'PartKeepr.FootprintBundle.Entity.Footprint'
                }
                    
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.FootprintBundle.Entity.FootprintImage'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.FootprintBundle.Entity.FootprintImage', '');

Ext.define('PartKeepr.FrontendBundle.Entity.GridPreset', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.FrontendBundle.Entity.GridPreset',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'grid', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'configuration', type: 'string', validators: []},
                { name: 'gridDefault', type: 'boolean', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/grid_presets'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.FrontendBundle.Entity.GridPreset', '');

Ext.define('PartKeepr.ImageBundle.Entity.CachedImage', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ImageBundle.Entity.CachedImage',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'originalId', type: 'int', validators: []},
                { name: 'originalType', type: 'string', validators: []},
                { name: 'cacheFile', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.ImageBundle.Entity.CachedImage'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ImageBundle.Entity.CachedImage', '');

Ext.define('PartKeepr.ImageBundle.Entity.Image', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ImageBundle.Entity.Image',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.ImageBundle.Entity.Image'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ImageBundle.Entity.Image', '');

Ext.define('PartKeepr.ImageBundle.Entity.TempImage', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ImageBundle.Entity.TempImage',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.ImageBundle.Entity.TempImage'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ImageBundle.Entity.TempImage', '');

Ext.define('PartKeepr.ImportBundle.Entity.ImportPreset', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ImportBundle.Entity.ImportPreset',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'baseEntity', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'configuration', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/import_presets'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ImportBundle.Entity.ImportPreset', '');

Ext.define('PartKeepr.ManufacturerBundle.Entity.Manufacturer', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ManufacturerBundle.Entity.Manufacturer',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'address', type: 'string', allowNull: true, validators: []},
                { name: 'url', type: 'string', allowNull: true, validators: []},
                { name: 'email', type: 'string', allowNull: true, validators: []},
                { name: 'comment', type: 'string', allowNull: true, validators: []},
                { name: 'phone', type: 'string', allowNull: true, validators: []},
                { name: 'fax', type: 'string', allowNull: true, validators: []}
                        
    ],

        hasMany: [
            {
        name: 'icLogos',
        associationKey: 'icLogos',
        model: 'PartKeepr.ManufacturerBundle.Entity.ManufacturerICLogo'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/manufacturers'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ManufacturerBundle.Entity.Manufacturer', '');

Ext.define('PartKeepr.ManufacturerBundle.Entity.ManufacturerICLogo', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ManufacturerBundle.Entity.ManufacturerICLogo',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                            ,
                            { name: 'manufacturer',
                reference: 'PartKeepr.ManufacturerBundle.Entity.Manufacturer',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.ManufacturerBundle.Entity.ManufacturerICLogo'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ManufacturerBundle.Entity.ManufacturerICLogo', '');

Ext.define('PartKeepr.PartBundle.Entity.MetaPartParameterCriteria', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.MetaPartParameterCriteria',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'partParameterName', type: 'string', validators: []},
                { name: 'operator', type: 'string', validators: []},
                { name: 'value', type: 'number', allowNull: true, validators: []},
                { name: 'normalizedValue', type: 'number', allowNull: true, validators: []},
                { name: 'stringValue', type: 'string', validators: []},
                { name: 'valueType', type: 'string', validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'siPrefix',
                reference: 'PartKeepr.SiPrefixBundle.Entity.SiPrefix',
            allowBlank: true                        },
                            { name: 'unit',
                reference: 'PartKeepr.UnitBundle.Entity.Unit',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.PartBundle.Entity.MetaPartParameterCriteria'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.MetaPartParameterCriteria', '');

Ext.define('PartKeepr.PartBundle.Entity.Part', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.Part',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: [{"type":"presence","message":"This value should not be blank."}]},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'comment', type: 'string', validators: []},
                { name: 'stockLevel', type: 'int', persist: false, validators: []},
                { name: 'minStockLevel', type: 'int', validators: []},
                { name: 'averagePrice', type: 'number', persist: false, validators: []},
                { name: 'status', type: 'string', allowNull: true, validators: []},
                { name: 'needsReview', type: 'boolean', validators: []},
                { name: 'partCondition', type: 'string', allowNull: true, validators: []},
                { name: 'productionRemarks', type: 'string', allowNull: true, validators: []},
                { name: 'createDate', type: 'date', allowNull: true, persist: false, validators: []},
                { name: 'internalPartNumber', type: 'string', allowNull: true, validators: []},
                { name: 'removals', type: 'boolean', persist: false, validators: []},
                { name: 'lowStock', type: 'boolean', persist: false, validators: []},
                { name: 'metaPart', type: 'boolean', validators: []}
                            ,
                            { name: 'category',
                reference: 'PartKeepr.PartBundle.Entity.PartCategory',
            allowBlank: false                        },
                            { name: 'footprint',
                reference: 'PartKeepr.FootprintBundle.Entity.Footprint',
            allowBlank: true                        },
                            { name: 'partUnit',
                reference: 'PartKeepr.PartBundle.Entity.PartMeasurementUnit',
            allowBlank: true                        },
                            { name: 'storageLocation',
                reference: 'PartKeepr.StorageLocationBundle.Entity.StorageLocation',
            allowBlank: true                        }
                            
    ],

        hasMany: [
            {
        name: 'manufacturers',
        associationKey: 'manufacturers',
        model: 'PartKeepr.PartBundle.Entity.PartManufacturer'
        },
            {
        name: 'distributors',
        associationKey: 'distributors',
        model: 'PartKeepr.PartBundle.Entity.PartDistributor'
        },
            {
        name: 'attachments',
        associationKey: 'attachments',
        model: 'PartKeepr.PartBundle.Entity.PartAttachment'
        },
            {
        name: 'stockLevels',
        associationKey: 'stockLevels',
        model: 'PartKeepr.StockBundle.Entity.StockEntry'
        },
            {
        name: 'parameters',
        associationKey: 'parameters',
        model: 'PartKeepr.PartBundle.Entity.PartParameter'
        },
            {
        name: 'metaPartParameterCriterias',
        associationKey: 'metaPartParameterCriterias',
        model: 'PartKeepr.PartBundle.Entity.MetaPartParameterCriteria'
        },
            {
        name: 'projectParts',
        associationKey: 'projectParts',
        model: 'PartKeepr.ProjectBundle.Entity.ProjectPart'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/parts'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.Part', '');

Ext.define('PartKeepr.PartBundle.Entity.PartAttachment', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartAttachment',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []},
                { name: 'isImage', type: 'boolean', allowNull: true, validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.PartBundle.Entity.PartAttachment'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartAttachment', '');

Ext.define('PartKeepr.PartBundle.Entity.PartCategory', {
    extend: 'PartKeepr.data.HydraTreeModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartCategory',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'lft', type: 'int', validators: []},
                { name: 'rgt', type: 'int', validators: []},
                { name: 'lvl', type: 'int', validators: []},
                { name: 'root', type: 'int', allowNull: true, validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'categoryPath', type: 'string', allowNull: true, validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/part_categories'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartCategory', '');

Ext.define('PartKeepr.PartBundle.Entity.PartDistributor', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartDistributor',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'orderNumber', type: 'string', allowNull: true, validators: []},
                { name: 'packagingUnit', type: 'int', validators: []},
                { name: 'price', type: 'number', allowNull: true, validators: []},
                { name: 'currency', type: 'string', allowNull: true, validators: []},
                { name: 'sku', type: 'string', allowNull: true, validators: []},
                { name: 'ignoreForReports', type: 'boolean', allowNull: true, validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'distributor',
                reference: 'PartKeepr.DistributorBundle.Entity.Distributor',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.PartBundle.Entity.PartDistributor'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartDistributor', '');

Ext.define('PartKeepr.PartBundle.Entity.PartManufacturer', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartManufacturer',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'partNumber', type: 'string', allowNull: true, validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'manufacturer',
                reference: 'PartKeepr.ManufacturerBundle.Entity.Manufacturer',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.PartBundle.Entity.PartManufacturer'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartManufacturer', '');

Ext.define('PartKeepr.PartBundle.Entity.PartMeasurementUnit', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartMeasurementUnit',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: [{"type":"presence","message":"partMeasurementUnit.name.not_blank"}]},
                { name: 'shortName', type: 'string', validators: [{"type":"presence","message":"partMeasurementUnit.shortName.not_blank"}]},
                { name: 'default', type: 'boolean', validators: []}
                        
    ],

        hasMany: [
            {
        name: 'parts',
        associationKey: 'parts',
        model: 'PartKeepr.PartBundle.Entity.Part'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/part_measurement_units'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartMeasurementUnit', '');

Ext.define('PartKeepr.PartBundle.Entity.PartParameter', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.PartBundle.Entity.PartParameter',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', validators: []},
                { name: 'value', type: 'number', allowNull: true, validators: []},
                { name: 'normalizedValue', type: 'number', allowNull: true, validators: []},
                { name: 'maxValue', type: 'number', allowNull: true, validators: []},
                { name: 'normalizedMaxValue', type: 'number', allowNull: true, validators: []},
                { name: 'minValue', type: 'number', allowNull: true, validators: []},
                { name: 'normalizedMinValue', type: 'number', allowNull: true, validators: []},
                { name: 'stringValue', type: 'string', validators: []},
                { name: 'valueType', type: 'string', validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'unit',
                reference: 'PartKeepr.UnitBundle.Entity.Unit',
            allowBlank: true                        },
                            { name: 'siPrefix',
                reference: 'PartKeepr.SiPrefixBundle.Entity.SiPrefix',
            allowBlank: true                        },
                            { name: 'minSiPrefix',
                reference: 'PartKeepr.SiPrefixBundle.Entity.SiPrefix',
            allowBlank: true                        },
                            { name: 'maxSiPrefix',
                reference: 'PartKeepr.SiPrefixBundle.Entity.SiPrefix',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.PartBundle.Entity.PartParameter'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.PartBundle.Entity.PartParameter', '');

Ext.define('PartKeepr.ProjectBundle.Entity.Project', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.Project',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []}
                            ,
                            { name: 'user',
                reference: 'PartKeepr.AuthBundle.Entity.User',
            allowBlank: true                        }
                            
    ],

        hasMany: [
            {
        name: 'parts',
        associationKey: 'parts',
        model: 'PartKeepr.ProjectBundle.Entity.ProjectPart'
        },
            {
        name: 'attachments',
        associationKey: 'attachments',
        model: 'PartKeepr.ProjectBundle.Entity.ProjectAttachment'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/projects'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.Project', '');

Ext.define('PartKeepr.ProjectBundle.Entity.ProjectAttachment', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ProjectAttachment',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                            ,
                            { name: 'project',
                reference: 'PartKeepr.ProjectBundle.Entity.Project',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/project_attachments'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ProjectAttachment', '');

Ext.define('PartKeepr.ProjectBundle.Entity.ProjectPart', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ProjectPart',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'quantity', type: 'int', validators: []},
                { name: 'remarks', type: 'string', allowNull: true, validators: []},
                { name: 'overageType', type: 'string', validators: []},
                { name: 'overage', type: 'int', validators: []},
                { name: 'lotNumber', type: 'string', validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: false        ,byReference: true                },
                            { name: 'project',
                reference: 'PartKeepr.ProjectBundle.Entity.Project',
            allowBlank: false                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/project_parts'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ProjectPart', '');

Ext.define('PartKeepr.ProjectBundle.Entity.ProjectRun', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ProjectRun',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'runDateTime', type: 'date', validators: []},
                { name: 'quantity', type: 'int', validators: []}
                            ,
                            { name: 'project',
                reference: 'PartKeepr.ProjectBundle.Entity.Project',
            allowBlank: true                        }
                            
    ],

        hasMany: [
            {
        name: 'parts',
        associationKey: 'parts',
        model: 'PartKeepr.ProjectBundle.Entity.ProjectRunPart'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/project_runs'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ProjectRun', '');

Ext.define('PartKeepr.ProjectBundle.Entity.ProjectRunPart', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ProjectRunPart',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'quantity', type: 'int', validators: []},
                { name: 'lotNumber', type: 'string', validators: []}
                            ,
                            { name: 'projectRun',
                reference: 'PartKeepr.ProjectBundle.Entity.ProjectRun',
            allowBlank: true                        },
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.ProjectBundle.Entity.ProjectRunPart'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ProjectRunPart', '');

Ext.define('PartKeepr.ProjectBundle.Entity.Report', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.Report',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', allowNull: true, validators: []},
                { name: 'createDateTime', type: 'date', validators: []}
                        
    ],

        hasMany: [
            {
        name: 'reportProjects',
        associationKey: 'reportProjects',
        model: 'PartKeepr.ProjectBundle.Entity.ReportProject'
        },
            {
        name: 'reportParts',
        associationKey: 'reportParts',
        model: 'PartKeepr.ProjectBundle.Entity.ReportPart'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/reports'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.Report', '');

Ext.define('PartKeepr.ProjectBundle.Entity.ReportPart', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ReportPart',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'quantity', type: 'int', validators: []}
                            ,
                            { name: 'report',
                reference: 'PartKeepr.ProjectBundle.Entity.Report',
            allowBlank: true                        },
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'distributor',
                reference: 'PartKeepr.DistributorBundle.Entity.Distributor',
            allowBlank: true                        }
                            
    ],

        hasMany: [
            {
        name: 'subParts',
        associationKey: 'subParts',
        model: 'PartKeepr.PartBundle.Entity.Part'
        },
            {
        name: 'projectParts',
        associationKey: 'projectParts',
        model: 'PartKeepr.ProjectBundle.Entity.ProjectPart'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/project_report_parts'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ReportPart', '');

Ext.define('PartKeepr.ProjectBundle.Entity.ReportProject', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.ProjectBundle.Entity.ReportProject',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'quantity', type: 'int', validators: []}
                            ,
                            { name: 'report',
                reference: 'PartKeepr.ProjectBundle.Entity.Report',
            allowBlank: true                        },
                            { name: 'project',
                reference: 'PartKeepr.ProjectBundle.Entity.Project',
            allowBlank: false                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/project_report_projects'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.ProjectBundle.Entity.ReportProject', '');

Ext.define('PartKeepr.SiPrefixBundle.Entity.SiPrefix', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.SiPrefixBundle.Entity.SiPrefix',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'prefix', type: 'string', validators: [{"type":"presence","message":"siprefix.prefix.not_blank"}]},
                { name: 'symbol', type: 'string', validators: []},
                { name: 'exponent', type: 'int', validators: []},
                { name: 'base', type: 'int', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/si_prefixes'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.SiPrefixBundle.Entity.SiPrefix', '');

Ext.define('PartKeepr.StatisticBundle.Entity.StatisticSnapshot', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.StatisticBundle.Entity.StatisticSnapshot',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'dateTime', type: 'date', validators: []},
                { name: 'parts', type: 'int', validators: []},
                { name: 'categories', type: 'int', validators: []}
                        
    ],

        hasMany: [
            {
        name: 'units',
        associationKey: 'units',
        model: 'PartKeepr.StatisticBundle.Entity.StatisticSnapshotUnit'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.StatisticBundle.Entity.StatisticSnapshot'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StatisticBundle.Entity.StatisticSnapshot', '');

Ext.define('PartKeepr.StatisticBundle.Entity.StatisticSnapshotUnit', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.StatisticBundle.Entity.StatisticSnapshotUnit',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'stockLevel', type: 'int', validators: []}
                            ,
                            { name: 'statisticSnapshot',
                reference: 'PartKeepr.StatisticBundle.Entity.StatisticSnapshot',
            allowBlank: true                        },
                            { name: 'partUnit',
                reference: 'PartKeepr.PartBundle.Entity.PartMeasurementUnit',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.StatisticBundle.Entity.StatisticSnapshotUnit'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StatisticBundle.Entity.StatisticSnapshotUnit', '');

Ext.define('PartKeepr.StockBundle.Entity.StockEntry', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.StockBundle.Entity.StockEntry',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'stockLevel', type: 'int', validators: []},
                { name: 'price', type: 'number', allowNull: true, validators: []},
                { name: 'dateTime', type: 'date', validators: []},
                { name: 'correction', type: 'boolean', validators: []},
                { name: 'comment', type: 'string', allowNull: true, validators: []}
                            ,
                            { name: 'part',
                reference: 'PartKeepr.PartBundle.Entity.Part',
            allowBlank: true                        },
                            { name: 'user',
                reference: 'PartKeepr.AuthBundle.Entity.User',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/stock_entries'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StockBundle.Entity.StockEntry', '');

Ext.define('PartKeepr.StorageLocationBundle.Entity.StorageLocation', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.StorageLocationBundle.Entity.StorageLocation',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []}
                            ,
                            { name: 'category',
                reference: 'PartKeepr.StorageLocationBundle.Entity.StorageLocationCategory',
            allowBlank: true                        }
                                        ,
                            { name: 'image',
                reference: 'PartKeepr.StorageLocationBundle.Entity.StorageLocationImage'
                }
                    
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/storage_locations'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StorageLocationBundle.Entity.StorageLocation', '');

Ext.define('PartKeepr.StorageLocationBundle.Entity.StorageLocationCategory', {
    extend: 'PartKeepr.data.HydraTreeModel',
    alias: 'schema.PartKeepr.StorageLocationBundle.Entity.StorageLocationCategory',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'lft', type: 'int', validators: []},
                { name: 'rgt', type: 'int', validators: []},
                { name: 'lvl', type: 'int', validators: []},
                { name: 'root', type: 'int', allowNull: true, validators: []},
                { name: 'name', type: 'string', validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'categoryPath', type: 'string', allowNull: true, validators: []}
                        
    ],

        hasMany: [
            {
        name: 'storageLocations',
        associationKey: 'storageLocations',
        model: 'PartKeepr.StorageLocationBundle.Entity.StorageLocation'
        }
        ],
    
    
    proxy: {
        type: "Hydra",
        url: '/api/storage_location_categories'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StorageLocationBundle.Entity.StorageLocationCategory', '');

Ext.define('PartKeepr.StorageLocationBundle.Entity.StorageLocationImage', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.StorageLocationBundle.Entity.StorageLocationImage',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                                    ,
                            { name: 'storageLocation',
                reference: 'PartKeepr.StorageLocationBundle.Entity.StorageLocation'
                }
                    
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.StorageLocationBundle.Entity.StorageLocationImage'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.StorageLocationBundle.Entity.StorageLocationImage', '');

Ext.define('PartKeepr.SystemPreferenceBundle.Entity.SystemPreference', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.SystemPreferenceBundle.Entity.SystemPreference',

    idProperty: "@id",
    fields: [
                { name: 'preferenceKey', type: 'string', validators: []},
                { name: 'preferenceValue', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/system_preferences'
                , ignoreIds: true
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.SystemPreferenceBundle.Entity.SystemPreference', '');

Ext.define('PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDay', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDay',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/tip_of_the_days'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDay', '');

Ext.define('PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDayHistory', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDayHistory',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: []}
                            ,
                            { name: 'user',
                reference: 'PartKeepr.AuthBundle.Entity.User',
            allowBlank: true                        }
                            
    ],

    
    
    proxy: {
        type: "Hydra",
        url: '/api/tip_of_the_day_histories'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.TipOfTheDayBundle.Entity.TipOfTheDayHistory', '');

Ext.define('PartKeepr.UnitBundle.Entity.Unit', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.UnitBundle.Entity.Unit',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'name', type: 'string', validators: [{"type":"presence","message":"unit.name.not_blank"}]},
                { name: 'symbol', type: 'string', validators: [{"type":"presence","message":"unit.symbol.not_blank"}]}
                        
    ],

    
        manyToMany: {
            prefixes: {
            type: 'PartKeepr.SiPrefixBundle.Entity.SiPrefix',
            role: 'prefixes',
            field: '@id',
            right: true
        }         },
    
    proxy: {
        type: "Hydra",
        url: '/api/units'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.UnitBundle.Entity.Unit', '');

Ext.define('PartKeepr.UploadedFileBundle.Entity.TempUploadedFile', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.UploadedFileBundle.Entity.TempUploadedFile',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.UploadedFileBundle.Entity.TempUploadedFile'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.UploadedFileBundle.Entity.TempUploadedFile', '');

Ext.define('PartKeepr.UploadedFileBundle.Entity.UploadedFile', {
    extend: 'PartKeepr.data.HydraModel',
    alias: 'schema.PartKeepr.UploadedFileBundle.Entity.UploadedFile',

    idProperty: "@id",
    fields: [
                { name: '@id', type: 'string', validators: []},
                { name: 'type', type: 'string', validators: []},
                { name: 'filename', type: 'string', validators: []},
                { name: 'originalFilename', type: 'string', allowNull: true, validators: []},
                { name: 'mimetype', type: 'string', validators: []},
                { name: 'size', type: 'int', validators: []},
                { name: 'extension', type: 'string', allowNull: true, validators: []},
                { name: 'description', type: 'string', allowNull: true, validators: []},
                { name: 'created', type: 'date', validators: []}
                        
    ],

    
    
    proxy: {
        type: "Hydra",
        url: 'undefined:PartKeepr.UploadedFileBundle.Entity.UploadedFile'
            }
});

PartKeepr.Data.Store.ModelStore.addModel('PartKeepr.UploadedFileBundle.Entity.UploadedFile', '');
